﻿using Microsoft.AspNetCore.Mvc;

namespace Jatosha.Challenge.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HelloWorldController : ControllerBase
    {
        [HttpGet]
        public IActionResult SayHello()
        {
            return this.Ok("Hello World!");
        }
    }
}