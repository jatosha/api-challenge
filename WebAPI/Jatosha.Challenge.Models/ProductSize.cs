namespace Jatosha.Challenge.Models
{
    public enum ProductSize
    {
        Large,
        ExtraLarge,
        SuperExtraLarge
    }
}