using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Jatosha.Challenge.Models;

namespace Jatosha.Challenge.DataAccess.Repository
{
    public class DildoRepository : IProductRepository
    {
        public Task<IEnumerable<Product>> LoadAll()
        {
            IEnumerable<Product> products = new List<Product> {
                    new Product(Guid.NewGuid(), "The Widowmaker", ProductSize.Large),
                    new Product(Guid.NewGuid(), "Hand of Exhaust", ProductSize.ExtraLarge),
                    new Product(Guid.NewGuid(), "Doctor Screw", ProductSize.SuperExtraLarge),
                    new Product(Guid.NewGuid(), "The Inner Limits", ProductSize.Large),
                    new Product(Guid.NewGuid(), "The D-800", ProductSize.ExtraLarge),
                    new Product(Guid.NewGuid(), "Excalibur", ProductSize.SuperExtraLarge)
            };
            
            return Task.FromResult(products);
        }
    }
}