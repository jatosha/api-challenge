using System.Collections.Generic;
using System.Threading.Tasks;
using Jatosha.Challenge.Models;

namespace Jatosha.Challenge.DataAccess.Repository
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product>> LoadAll();
    }
}